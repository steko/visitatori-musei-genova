museige <- read.csv("museiGE_1996-2013.csv", as.is=TRUE, na="-")
col2cvt <- 2:19
museige[,col2cvt] <- lapply(museige[,col2cvt],function(x){as.numeric(x)})
options(scipen=999)
# flat structure would be: museum, year, visitors

require(ggplot2)
require(reshape2)
require(grid)
require(RColorBrewer)

memuseige <- melt(museige)

# remove spurious "X" in year data
levels(memuseige$variable) <- sapply(levels(memuseige$variable), function(x){gsub("X","", x)})


# total visits plot
ggplot(memuseige, aes(variable, value)) +
  geom_bar(stat="identity") +
  labs(title="Visitatori nei musei civici e Acquario di Genova 1996-2013",
       x="Anno",
       y="Visitatori") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1))
ggsave("museige_1996-2013.png")

# total visits plot by museum
colourCount = length(unique(memuseige$Museo))
getPalette = colorRampPalette(brewer.pal(9, "Dark2"))
ggplot(acquario, aes(variable, value, fill=Museo)) +
  geom_bar(stat="identity") +
  scale_fill_manual(values = colorRampPalette(brewer.pal(12, "Set2"))(colourCount)) +
  labs(title="Visitatori nei musei civici e Acquario di Genova 1996-2013",
       x="Anno",
       y="Visitatori")

# look at single museums
pegli <- subset(memuseige, grepl("Archeologia", Museo))
acquario <- subset(memuseige, grepl("Acquario", Museo))
ggplot(acquario, aes(variable, value)) +
  geom_bar(stat="identity") +
  labs(title="Visitatori Acquario di Genova 1994-2013",
       x="Anno",
       y="Visitatori") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1))
ggsave("museige-acquario_1994-2013.png")

# exclude a museum #1
noacquario <- subset(memuseige, memuseige$Museo!="Acquario di Genova")
ggplot(noacquario, aes(variable, value)) + geom_bar(stat="identity") +
  labs(title="Visitatori Musei civici di Genova 1996-2013",
       x="Anno",
       y="Visitatori") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1))
ggsave("museige-noacquario_1996-2013.png")

# exclude a museum #2
nogalata <- subset(noacquario, noacquario$Museo!="Museo del Mare (Galata)")
ggplot(nogalata, aes(variable, value)) + geom_bar(stat="identity") +
  labs(title="Visitatori Musei di Genova, escluso Galata Museo del mare 1996-2013",
       x="Anno",
       y="Visitatori") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1))
ggsave("museige-nogalata_1996-2013.png")

# trends for most museums are not upswing, total visitors are rising because more museums are added
ggplot(memuseige, aes(x=variable, y=value, group=Museo, colour=Museo)) +
  geom_line(arrow = arrow(length=unit(0.5, "cm"))) +
  #scale_colour_manual(values=rep(mixed, length.out=nrow(memuseige))) +
  facet_wrap(~ Museo, ncol=1, scales="free_y") +
  theme_bw() +
  theme(legend.position = "none") +
  labs(title="Visitatori nei singoli musei di Genova 1996-2013",
       x="Anno",
       y="Visitatori")

ggsave("museige-dettaglio_1996-2013.png", height=70, width= 25, units="cm")
