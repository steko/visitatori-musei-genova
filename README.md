# Musei di Genova

How many people visit museums in Genoa (since 1996).

Based on data published by Comune di Genova [in XLS
format](http://statistica.comune.genova.it/pubblicazioni/download/annuario/Annuario%202014/Tavole_annuario_2014.zip)
(warning, link to a large ZIP file).

A summary view in Italian is at <http://steko.iosa.it/2015/04/quanti-visitatori-nei-musei-di-genova/> for now.
